package bootstrap;

import model.Customer;
import model.Product;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DataLoader {

    public List<Customer> loadData() {
        return loadCustomers();
    }

    private List<Customer> loadCustomers() {
        List<Customer> customers = new ArrayList<>();
        List<Product> products = loadProducts();

        Customer customer1 = new Customer();
        customer1.setId(1);
        customer1.setFirstName("John");
        customer1.setLastName("Doe");
        customer1.setAddress("Str. Liberty No. 5");
        customer1.setOrderDate(Date.valueOf(LocalDate.now()));
        customer1.setProducts(products.subList(0, 3));

        Customer customer2 = new Customer();
        customer2.setId(2);
        customer2.setFirstName("Dave");
        customer2.setLastName("Black");
        customer2.setAddress("Str. Energy No. 5");
        customer2.setOrderDate(Date.valueOf(LocalDate.now()));
        customer2.setProducts(products.subList(3, products.size() - 1));

        customers.add(customer1);
        customers.add(customer2);

        return customers;
    }

    private List<Product> loadProducts() {
        List<Product> products = new ArrayList<>();

        products.add(new Product(1L, "Laptop", 500));
        products.add(new Product(2L, "Monitor", 300));
        products.add(new Product(3L, "Mouse", 100));
        products.add(new Product(4L, "Keyboard", 110));
        products.add(new Product(5L, "SSD", 220));
        products.add(new Product(6L, "Jukebox", 260));

        return products;
    }
}
