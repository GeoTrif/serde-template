package app;

import bootstrap.DataLoader;
import model.Customer;
import model.SerializationType;
import service.DeserializationService;
import service.SerializationService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class MainApp {

    private static final DataLoader DATA_LOADER = new DataLoader();
    private static final SerializationService SERIALIZATION_SERVICE = new SerializationService();
    private static final DeserializationService DESERIALIZATION_SERVICE = new DeserializationService();

    private static final SerializationType SERIALIZATION_TYPE = SerializationType.JSON;

    public static void main(String[] args) {
        List<Customer> customers = DATA_LOADER.loadData();
        serializeAndDeserializeFromString(customers);

        File serFile = serializeObjectsToPayloadsIntoFile(customers);
        deserializePayloadsFromFileIntoObjects(serFile.getPath());
    }

    private static void serializeAndDeserializeFromString(List<Customer> customers) {
        List<Customer> deserializedPayloads = new ArrayList<>();

        Queue<String> payloadsToBeProcessed = SERIALIZATION_SERVICE.serializeObjectAndPutInQueue(customers, SERIALIZATION_TYPE);

        System.out.println(payloadsToBeProcessed);

        while (!payloadsToBeProcessed.isEmpty()) {
            Customer customer = DESERIALIZATION_SERVICE.deserializePayload(payloadsToBeProcessed.poll(), SERIALIZATION_TYPE);
            deserializedPayloads.add(customer);
        }

        System.out.println(deserializedPayloads);
    }

    private static File serializeObjectsToPayloadsIntoFile(List<Customer> customers) {
        File serFile = SERIALIZATION_SERVICE.serializeObjectAndPutInFile(customers, SERIALIZATION_TYPE);

        return serFile;
    }

    private static void deserializePayloadsFromFileIntoObjects(String serFilePath) {
        try {
            List<String> linesFromFile = Files.readAllLines(Paths.get(serFilePath));
            linesFromFile.forEach(line -> System.out.println(DESERIALIZATION_SERVICE.deserializePayload(line, SERIALIZATION_TYPE)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
