package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import model.Customer;
import model.SerializationType;

public class DeserializationService {

    private final ObjectMapper objectMapper;
    private final XmlMapper xmlMapper;
    private final YAMLMapper yamlMapper;

    public DeserializationService() {
        this.objectMapper = new ObjectMapper();
        this.xmlMapper = new XmlMapper();
        this.yamlMapper = new YAMLMapper();
    }

    public Customer deserializePayload(String payload, SerializationType payloadType) {
        Customer customer = new Customer();

        try {
            customer = deserializePayloadByType(payload, payloadType);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return customer;
    }

    private Customer deserializePayloadByType(String payload, SerializationType payloadType) throws JsonProcessingException {
        Customer customer = new Customer();

        switch (payloadType) {
            case JSON:
                customer = objectMapper.readValue(payload, Customer.class);
                break;
            case XML:
                customer = xmlMapper.readValue(payload, Customer.class);
                break;
            case YAML:
                customer = yamlMapper.readValue(payload, Customer.class);
                break;
        }

        return customer;
    }
}
