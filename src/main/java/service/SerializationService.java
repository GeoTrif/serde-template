package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import model.Customer;
import model.SerializationType;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;

public class SerializationService {

    private static final String XML_PAYLOADS_FILE_PATHNAME = "xmlPayloads.ser";
    private static final String GENERIC_PAYLOADS_FILE_PATHNAME = "payloads.ser";
    private static final String JSON_PAYLOADS_FILE_PATHNAME = "jsonPayloads.ser";
    private static final String YAML_PAYLOADS_FILE_PATHNAME = "yamlPayloads.ser";

    private final ObjectMapper objectMapper;
    private final XmlMapper xmlMapper;
    private final YAMLMapper yamlMapper;

    public SerializationService() {
        this.objectMapper = new ObjectMapper();
        this.xmlMapper = new XmlMapper();
        this.yamlMapper = new YAMLMapper();
    }

    public Queue<String> serializeObjectAndPutInQueue(List<Customer> customerList, SerializationType serializationType) {
        Queue<String> payloadsQueue = new ArrayDeque<>();

        for (Customer customer : customerList) {
            try {
                payloadsQueue.offer(serializeObjectBySerializationType(customer, serializationType));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }

        return payloadsQueue;
    }

    public File serializeObjectAndPutInFile(List<Customer> customers, SerializationType serializationType) {
        File file = createFileBasedOnSerializationType(serializationType);
        customers.forEach(customer -> savePayloadsToFile(file, customer, serializationType));

        return file;
    }

    private String serializeObjectBySerializationType(Customer customer, SerializationType serializationType) throws JsonProcessingException {
        String payload = "";

        switch (serializationType) {
            case JSON:
                payload = objectMapper.writeValueAsString(customer);
                break;
            case XML:
                payload = xmlMapper.writeValueAsString(customer);
                break;
            case YAML:
                payload = yamlMapper.writeValueAsString(customer);
                break;
        }

        return payload;
    }

    private File createFileBasedOnSerializationType(SerializationType serializationType) {
        switch (serializationType) {
            case JSON:
                return new File(JSON_PAYLOADS_FILE_PATHNAME);
            case XML:
                return new File(XML_PAYLOADS_FILE_PATHNAME);
            case YAML:
                return new File(YAML_PAYLOADS_FILE_PATHNAME);
        }

        return new File(GENERIC_PAYLOADS_FILE_PATHNAME);
    }

    private void savePayloadsToFile(File file, Customer customer, SerializationType serializationType) {
        if (file.isFile()) {
            createOrAppendToFile(file, customer, serializationType, StandardOpenOption.APPEND);
        } else {
            createOrAppendToFile(file, customer, serializationType, StandardOpenOption.CREATE_NEW);
        }
    }

    private void createOrAppendToFile(File file, Customer customer, SerializationType serializationType, StandardOpenOption openOption) {
        try {
            Files.write(file.toPath(), (serializeObjectBySerializationType(customer, serializationType) + "\n").getBytes(StandardCharsets.UTF_8),
                    openOption);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
