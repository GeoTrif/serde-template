package model;

public enum SerializationType {
    JSON, XML, YAML
}
